const changeThemeBtn = document.getElementById('change-theme');
const cssLink = document.getElementById('theme');
const currentThemeText = document.querySelector('.current-theme');
const availableThemes = ['general-theme', 'custom-theme'];

const handleSetCurrentTheme = () => {
    const theme = localStorage.getItem('theme');
    if (theme) {
        currentThemeText.innerText = `Current theme: ${theme.split('-')[0].toUpperCase()}`;
        return cssLink.href = `CSS/${theme}.css`;
    }

    return localStorage.setItem('theme', availableThemes[0]);
}

const handleChangeTheme = () => {
    let theme = localStorage.getItem('theme');
    const nextTheme = availableThemes.find(t => t !== theme);
    cssLink.href = `CSS/${nextTheme}.css`;
    localStorage.setItem('theme', nextTheme);
    currentThemeText.innerText = `Current theme: ${nextTheme.split('-')[0].toUpperCase()}`;
}


changeThemeBtn.addEventListener('click', handleChangeTheme);
handleSetCurrentTheme();
